import { Component, OnInit } from '@angular/core';
import { ProductService } from './products/shared/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pageTitle = 'Product Management';
  cartProductCount = 0;
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.getCartProductCount().subscribe({
      next: productCount => {
        this.cartProductCount = productCount || 0;
      },
    });
  }
}
