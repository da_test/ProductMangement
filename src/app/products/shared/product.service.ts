import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IProduct } from './product.model'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private rootURL = '/api';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.rootURL + '/products')
      .pipe(
        catchError(this.handleError)
      );
  }

  getCartProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.rootURL + '/cart')
      .pipe(
        catchError(this.handleError)
      );
  }

  addProductToCart(product: IProduct): Observable<number> {
    return this.http.post<number>(this.rootURL + '/cart', product)
      .pipe(
        catchError(this.handleError)
      );
  }

  removeProductFromCart(productId: number): Observable<number> {
    const url = `${ this.rootURL }/cart/${ productId }`;
    return this.http.delete<number>(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateProductQuantity(product: IProduct): Observable<number> {
    return this.http.put<number>(this.rootURL + '/cart', product)
      .pipe(
        catchError(this.handleError)
      );
  }

  getCartProductCount(): Observable<number> {
    return this.http.get<number>(this.rootURL + '/cartItemCount')
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(err: HttpErrorResponse): Observable<never> {

    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${ err.error.message }`;
    } else {
      errorMessage = `Server returned code: ${ err.status }, error message is: ${ err.message }`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
