export interface IProduct {
  productId: number;
  productName: string;
  productCategory: string;
  price: number;
  cartCount: number;
}
