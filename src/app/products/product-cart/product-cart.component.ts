import { Component, OnInit } from '@angular/core';
import { IProduct } from '../shared/product.model';
import { ProductService } from '../shared/product.service';

@Component({
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.css']
})
export class ProductCartComponent implements OnInit {
  pageTitle = 'Cart Details';
  errorMessage = '';
  cartProducts: IProduct[] = [];
  maxQuantityCount: number = 10;
  quantityCount: number[] = [];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.getCartProducts().subscribe({
      next: products => {
        this.setQuantityDrowdownArray();
        this.cartProducts = products;
      },
      error: err => this.errorMessage = err
    });
  }

  removeFromCart(productID: number): void {
    this.productService.removeProductFromCart(productID).subscribe({
      next: cartItemCount => {
        // update here the cart item count
      },
      error: err => this.errorMessage = err
    });
  }

  quantityUpdated(product: IProduct) {
    this.productService.updateProductQuantity(product).subscribe({
      next: cartItemCount => {
        // update here the cart item count
      },
      error: err => this.errorMessage = err
    });
  }

  setQuantityDrowdownArray() {
    const qCount = [];
    for (let count = 1; count <= this.maxQuantityCount; count++) {
      qCount.push(count);
    }
    this.quantityCount = qCount;
  }
}
